using Gtk 4.0;
using Adw 1;

template $FlChannelMessages: Box {
  orientation: vertical;
  hexpand: true;
  vexpand: true;

  styles [
    "channel-messages",
  ]

  // Offline Indicator
  Adw.Banner {
    title: _("Offline");
    revealed: bind $not($network_monitor() as <$GNetworkMonitor>.network-available) as <bool>;
  }

  // Status Page
  Adw.StatusPage {
    title: _("No Channel Selected");
    description: _("Select a channel");
    icon-name: "system-search-symbolic";
    vexpand: true;
    hexpand: true;
    visible: bind $is_none(template.active-channel) as <bool>;
  }

  Box {
    styles [
      "view",
    ]

    visible: bind $is_some(template.active-channel) as <bool>;
    hexpand: true;
    orientation: vertical;

    Spinner {
      spinning: true;
      visible: bind template.loading;
    }

    Overlay {
      [overlay]
      Button {
        styles [
          "circular",
          "opaque",
          "scroll-to-bottom",
        ]

        icon-name: "go-bottom-symbolic";
        halign: end;
        valign: end;
        margin-bottom: 7;
        margin-end: 7;
        clicked => $scroll_down() swapped;
        visible: bind $not(template.sticky) as <bool>;
      }

      // Message List
      ScrolledWindow scrolled_window {
        styles [
          "undershoot-bottom"
        ]

        vexpand: true;
        hscrollbar-policy: never;
        edge-overshot => $handle_edge_reached() swapped;

        Adw.ClampScrollable {
          vexpand: false;
          hexpand: true;
          maximum-size: 800;
          tightening-threshold: 600;

          styles [
            "message-clamp",
          ]
          
          ListView list_view {
            styles [
              "message-list",
            ]

            model: bind $no_selection(template.active-channel as <$FlChannel>.timeline) as <SelectionModel>;
            hexpand: true;
            vexpand: false;
          }
        }
      }
    }
     
    Box {
      styles [
        "toolbar",
        "message-input-bar"
      ]

      orientation: horizontal;
      // Entry Box
      Adw.Clamp {
        maximum-size: 800;
        tightening-threshold: 600;

        Box {
          orientation: vertical;
          // Quote
          Box {
            vexpand-set: true;

            styles [
              "currently-replied-box"
            ]

            visible: bind $is_some(template.reply-message) as <bool>;

            Image {
              styles [
                "accent"
              ]

              icon-name: "mail-reply-sender-symbolic";
              width-request: 34;
            }

            Separator {
              styles [
                "accent"
              ]

              margin-start: 6;
              width-request: 2;
            }

            Grid {
              hexpand: true;
              margin-start: 12;
              margin-end: 12;

              Label {
                styles [
                  "heading",
                ]

                halign: start;
                label: bind template.reply-message as <$FlTextMessage>.sender as <$FlContact>.title;
                wrap: true;
                wrap-mode: word_char;
                lines: 1;
                ellipsize: end;

                layout {
                  row: 0;
                  column: 0;
                }
              }

              Label {
                styles [
                  "message-text",
                ]

                halign: fill;
                label: bind $attachment_placeholder(template.reply-message as <$FlTextMessage>.body) as <string>;
                wrap: true;
                wrap-mode: word_char;
                lines: 2;
                ellipsize: end;
                xalign: 0;

                layout {
                  row: 1;
                  column: 0;
                }
              }
            }

            Button {
              accessibility {
                label: C_("accessibility", "Remove the reply");
              }

              tooltip-text: C_("tooltip", "Remove reply");

              styles [
                "flat",
                "circular"
              ]

              valign: center;
              clicked => $remove_reply() swapped;
              icon-name: "window-close-symbolic";
            }
          }

          // Box for attachments
          Box {
            vexpand-set: true;
            visible: bind template.has-attachments;

            Button {
              accessibility {
                label: C_("accessibility", "Remove an attachment");
              }

              tooltip-text: C_("tooltip", "Remove attachment");

              styles [
                "flat",
              ]

              clicked => $remove_attachments() swapped;
              icon-name: "list-remove-symbolic";
            }

            Box box_attachments {
              orientation: vertical;
              hexpand: true;
            }
          }

          Box {
            spacing: 6;

            Button {
              accessibility {
                label: C_("accessibility", "Add an attachment");
              }

              styles [
                "circular",
              ]

              valign: end;
              tooltip-text: C_("tooltip", "Add attachment");
              clicked => $add_attachment() swapped;
              icon-name: "mail-attachment-symbolic";

              ShortcutController {
                scope: managed;

                Shortcut {
                  trigger: "<Control>plus";
                  action: "activate";
                }
              }
            }

            $FlTextEntry text_entry {
              accessibility {
                label: C_("accessibility", "Message input");
              }

              activate => $send_message() swapped;
              paste-file => $paste_file() swapped;
              paste-texture => $paste_texture() swapped;
              tooltip-text: C_("tooltip", "Message input");
            }

            Button button_send {
              styles [
                "circular",
                "suggested-action",
              ]

              sensitive: bind $and($or(template.has-attachments, $not(text_entry.is-empty) as <bool>) as <bool>, $network_monitor() as <$GNetworkMonitor>.network-available) as <bool>;

              accessibility {
                label: C_("accessibility", "Send");
              }

              icon-name: "send-symbolic";
              tooltip-text: C_("tooltip", "Send");
              valign: end;
              clicked => $send_message() swapped;
            }
          }
        }
      }
    }
  }
}
